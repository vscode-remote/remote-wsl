
import net from "net"
import { DaemonMessage } from "./daemon-message"

export class SimpleProtocol {

    onMessage: (message: DaemonMessage) => any;
    onClose: () => any;

    _byteProtocol: ByteProtocol;

    constructor(socket: net.Socket) {

        this.onMessage = null
        this.onClose = null

        this._byteProtocol = new ByteProtocol(socket)

        this._byteProtocol.onClose = () => {
            this.onClose && this.onClose();
        }

        this._byteProtocol.onMessage = e => {
            if (!this.onMessage) throw new Error("Incorrect usage!");
            this.onMessage(JSON.parse(e.toString()));
        }

    }

    send(message: DaemonMessage) {
        this._byteProtocol.send(Buffer.from(JSON.stringify(message)));
    }

}

class WriteBuffer {

    _data: Buffer[];
    _totalLength: number;

    constructor() {
        this._data = []
        this._totalLength = 0
    }

    add(header: Buffer, messageBuffer: Buffer) {

        const hasNotWritten = this._totalLength === 0

        this._data.push(header, messageBuffer)

        this._totalLength += header.length + messageBuffer.length

        return hasNotWritten

    }

    take() {
        const buffer = Buffer.concat(this._data, this._totalLength)
        this._data.length = 0
        this._totalLength = 0
        return buffer
    }
}

export class ByteProtocol {

    static _headerLen = 4;

    onMessage: (data: Buffer) => any;
    onClose: () => any;

    _socket: net.Socket;
    _writeBuffer: WriteBuffer;
    _chunks: Buffer[];

    _socketDataListener: (data: Buffer) => void;
    _socketCloseListener: () => void;

    constructor(socket: net.Socket) {

        this._socket = socket

        this._writeBuffer = new WriteBuffer()

        this._chunks = []

        this.onMessage = null
        this.onClose = null

        let len = 0

        const status = {
            readHead: true,
            bodyLen: -1
        }

        this._socketDataListener = (data: Buffer) => {

            this._chunks.push(data)

            len += data.length

            while (len > 0) {

                if (status.readHead) {

                    if (!(len >= ByteProtocol._headerLen)) {
                        break
                    }

                    {
                        const buffer = Buffer.concat(this._chunks);
                        status.bodyLen = buffer.readUInt32BE(0)
                        status.readHead = false

                        const body = buffer.slice(ByteProtocol._headerLen)

                        len = body.length
                        this._chunks = [body]
                    }

                }

                if (!status.readHead) {

                    if (!(len >= status.bodyLen)) {
                        break;
                    }

                    {
                        const buffer = Buffer.concat(this._chunks)
                        const body = buffer.slice(0, status.bodyLen)
                        const rest = buffer.slice(status.bodyLen)

                        len = rest.length
                        this._chunks = [rest]

                        status.bodyLen = -1
                        status.readHead = true

                        if (!this.onMessage) {
                            throw new Error("Incorrect usage!");
                        }

                        this.onMessage(body)
                    }

                }

            }

        }

        socket.on("data", this._socketDataListener)

        this._socketCloseListener = () => {
            if (this.onClose) {
                this.onClose()
            }
        }

        socket.once("close", this._socketCloseListener)

    }

    send(messageBuffer: Buffer) {
        const header = Buffer.allocUnsafe(ByteProtocol._headerLen)
        // @ts-ignore
        header.writeUInt32BE(messageBuffer.length, 0, true)
        this._writeSoon(header, messageBuffer)
    }

    _writeSoon(header: Buffer, messageBuffer: Buffer) {
        const hasNotWritten = this._writeBuffer.add(header, messageBuffer)
        if (hasNotWritten) {
            setImmediate(() => {
                if (!this._socket.destroyed) {
                    this._socket.write(this._writeBuffer.take())
                }
            })
        }
    }

}

export default {
    SimpleProtocol,
    ByteProtocol,
}
