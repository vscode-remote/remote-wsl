
import net from "net"
import ChildProcess from "child_process"
import fs from "fs"
import path from "path"
import Utils from "./utils"
import Protocol from "./protocol"

import { DaemonMessage } from "./daemon-message"

const debugPort = process.env.VSCODE_WSL_AGENT_DEBUG_PORT
const pipeName = process.env.VSCODE_WSL_PIPE_NAME
const extensionHostPid = parseInt(process.env.VSCODE_WSL_EXT_HOST_PID || "0", 10)
const extensionLocation = process.env.VSCODE_WSL_EXT_LOCATION
const appRoot = process.env.VSCODE_WSL_APP_ROOT
const enableTelemetry = process.env.VSCODE_WSL_ENABLE_TELEMETRY || false
const distro = process.env.VSCODE_WSL_DISTRO

export class ProgressMessage {

    type: "progressMessage";
    increment: number;
    message: string;

    constructor(increment: number, message: string) {
        this.increment = increment
        this.message = message
        this.type = "progressMessage"
    }

}

export class StdoutMessage {

    type: "stdoutMessage";
    data: string;

    constructor(data: string) {
        this.data = data
        this.type = "stdoutMessage"
    }

}

export class StderrMessage {

    type: "stderrMessage";
    data: string;

    constructor(data: string) {
        this.data = data
        this.type = "stderrMessage"
    }

}

export class PortResolvedMessage {

    type: "portResolved";
    ipAddresses: string[];
    port: number;

    constructor(ipAddresses: string[], port: number) {
        this.ipAddresses = ipAddresses
        this.port = port
        this.type = "portResolved"
    }

}

export class FatalErrorOccuredMessage {

    type: "fatalErrorOccured";
    message: string;

    constructor(errMessage: string) {
        this.message = errMessage
        this.type = "fatalErrorOccured"
    }

}

interface MessageSenderLike {
    send(message: DaemonMessage): any
}

let cachedMessage: DaemonMessage = null
let messageSenderList: MessageSenderLike[] = []

const closeMessageSender = (sender: MessageSenderLike) => {

    for (let i = 0; i < messageSenderList.length; i++) {
        if (messageSenderList[i] === sender) {
            messageSenderList.splice(i, 1)
        }
    }

    if (messageSenderList.length === 0) {
        process.exit(0)
    }

}

const addMessageToStdoutSender = () => {

    const sender: MessageSenderLike = new class {
        send(message: DaemonMessage) {
            process.stdout.write(JSON.stringify(message) + "\n");
        }
    }()

    messageSenderList.push(sender)

    let interval = setInterval(() => {
        try {
            process.kill(extensionHostPid, 0)
        } catch (s) {
            clearInterval(interval)
            closeMessageSender(sender)
        }
    }, 1000)

}

addMessageToStdoutSender()

const server = net.createServer((socket) => {

    const simpleprotocol = new Protocol.SimpleProtocol(socket)

    const sender = new class {
        send(message: DaemonMessage) {
            simpleprotocol.send(message);
        }
    }()

    messageSenderList.push(sender)

    simpleprotocol.onMessage = () => {
        throw new Error("Received unexpected message");
    }

    if (cachedMessage) {
        sender.send(cachedMessage)
    }

    socket.on("close", () => {
        closeMessageSender(sender)
    })

})

const serverErrCallback = (err: Error & { code: string }) => {

    if (err && err.code === "EADDRINUSE") {
        console.error(err)
        process.exit(0)
    }

    let _errCb = (err: Error) => {
        console.error(err)
        process.exit(0)
    }

    let socket = net.createConnection(pipeName, () => {

        socket.removeListener("error", _errCb)

        const simpleprotocol = new Protocol.SimpleProtocol(socket)

        simpleprotocol.onMessage = (message) => {
            sendMessage(message)
        }

        socket.on("close", () => {
            process.exit(0);
        })

    })

    socket.once("error", _errCb)

}

const sendMessage = (message: DaemonMessage) => {
    messageSenderList.forEach(sender => {
        sender.send(message)
    })
}

const isServerDataExists = (wslExecutablePath: string, distro: string, serverDataDir: string, commitID: string) => {

    let checkServerDataExistsCmd: string

    if (wslExecutablePath.endsWith("wsl.exe")) {
        const specifyDistroFlag = distro ? `-d ${distro}` : ""
        checkServerDataExistsCmd = `${wslExecutablePath} ${specifyDistroFlag} bash -c "[ -d ~/${serverDataDir}/bin/${commitID} ] && echo found || echo missing"`
    } else {
        checkServerDataExistsCmd = `${wslExecutablePath} [ -d ~/${serverDataDir}/bin/${commitID} ] && echo found || echo missing`
    }

    sendMessage(new StdoutMessage(`Probing if server is already installed: ${checkServerDataExistsCmd}`))

    const serverDataFound =
        ChildProcess
            .execSync(checkServerDataExistsCmd, {
                stdio: [null, null, null]
            })
            .toString()
            .indexOf("found") !== -1

    return serverDataFound

}

server.on("error", serverErrCallback)

server.listen(pipeName, async () => {

    let timer: NodeJS.Timer

    server.removeListener("error", serverErrCallback)

    let wslServerMessageCache = ""

    let progressValue = 0

    let wslServerMessage = ""

    let ipAddresses = []

    const resolveError = (errMessage: string) => {
        clearTimeout(timer)
        cachedMessage = new FatalErrorOccuredMessage(errMessage)
        sendMessage(cachedMessage)
    }

    const parseWslServerMessage = (wslServerStdout: string) => {

        for (let i = 0; i < wslServerStdout.length; i++) {

            const charCode = wslServerStdout.charCodeAt(i)

            if (charCode === 10) {  // \n

                const portMatch = wslServerMessageCache.match(/Extension host agent listening on (\d+)/)

                if (portMatch) {
                    clearTimeout(timer)
                    cachedMessage = new PortResolvedMessage(ipAddresses, parseInt(portMatch[1], 10))
                    sendMessage(cachedMessage)
                    return true
                }

                const ipAddrMatch = wslServerMessageCache.match(/IP Address: ([\d\.]+)/)
                if (ipAddrMatch) {
                    ipAddresses.push(ipAddrMatch[1])
                }

                reportProgress(wslServerMessageCache)

                wslServerMessageCache = ""

            } else if (charCode === 8) {  // Backspace

                if (wslServerMessageCache.charCodeAt(wslServerMessageCache.length - 1) !== 10) {

                    wslServerMessageCache = wslServerMessageCache.substr(0, wslServerMessageCache.length - 1)

                }

            } else {

                wslServerMessageCache += wslServerStdout.charAt(i)

            }

        }

        reportProgress(wslServerMessageCache)

        return false
    }

    const reportProgress = (_wslServerMessage: string) => {

        const progressIncrementMatch = _wslServerMessage.match(/(\d+)%$/)

        let progressIncrement = 0

        if (progressIncrementMatch) {

            const progressValueNow = Math.min(parseInt(progressIncrementMatch[1], 10), 100)

            if (progressValueNow === 0) {
                progressValue = 0
            }

            if (progressValueNow > progressValue) {
                progressIncrement = progressValueNow - progressValue
                progressValue = progressValueNow
            }

        }

        if (wslServerMessage !== _wslServerMessage || progressIncrement > 0) {
            wslServerMessage = _wslServerMessage
            sendMessage(new ProgressMessage(progressIncrement / 2, _wslServerMessage))
        }

    }

    const getWslServerResult = () => {

        let successful = false

        if (wslSeverMessageReceived) {

            wslSeverMessageReceived = false
            retry = 0

            if (notInited) {
                sendMessage(new ProgressMessage(0, "Installing WSL components"))
                notInited = false
            }

        } else if (++retry > 300) {
            resolveError(
                `VS Code Server for WSL failed to start. No messages received for ${Math.floor(90)}s`
            )
            return
        }

        if (wslServerStdoutCache.length) {

            sendMessage(new StdoutMessage(wslServerStdoutCache))

            successful = parseWslServerMessage(wslServerStdoutCache)

            wslServerStdoutCache = ""

        }

        if (wslServerStderrCache.length) {
            sendMessage(new StderrMessage(wslServerStderrCache))
            wslServerStderrCache = ""
        }

        return successful
    }

    let wslServerStdoutCache = ""
    let wslServerStderrCache = ""

    let wslSeverMessageReceived = false
    let retry = 0
    let notInited = true

    timer = setInterval(getWslServerResult, 300)

    const wslExecutablePath = Utils.getWSLExecutablePath()

    if (!wslExecutablePath || !fs.existsSync(wslExecutablePath)) {
        resolveError(
            `VS Code Server for WSL failed. '${wslExecutablePath}' not found.\n\nMake sure WSL is installed:\nhttps://aka.ms/vscode-remote/wsl/install-wsl`
        )
        return
    }

    const productConfig = Utils.getProductConfiguration(appRoot)

    const { commit: commitID, quality, serverDataFolderName } = productConfig

    if (commitID && !quality) {
        resolveError(`VSCode Client (${appRoot}) does not define a quality.`)
        return
    }

    const serverDataDir = serverDataFolderName || ".vscode-remote"

    let serverTarCacheLocation = ""

    if (commitID) {

        serverTarCacheLocation = Utils.getServerTarCacheLocation(productConfig)

        if (!fs.existsSync(serverTarCacheLocation)) {
            try {

                if (isServerDataExists(wslExecutablePath, distro, serverDataDir, commitID)) {
                    serverTarCacheLocation = ""
                    sendMessage(new StdoutMessage("Server already installed in WSL"))
                } else {

                    sendMessage(new StdoutMessage("No server install found on WSL, downloading server on client side..."))

                    let progressOld = 0

                    await Utils.donwloadBuild(productConfig, serverTarCacheLocation, (bytesDownloaded, max) => {

                        const progressNow = Math.ceil((50 * bytesDownloaded) / max)

                        if (progressNow > progressOld) {
                            const progressIncrement = progressNow - progressOld
                            sendMessage(new ProgressMessage(progressIncrement, "Downloading server..."))
                            progressOld = progressNow
                        }

                    })

                }

            } catch (err) {
                const message = new StderrMessage(`Unable to detect if server is already installed: ${err}`)
                sendMessage(message)
                serverTarCacheLocation = ""
            }
        }

        serverTarCacheLocation = serverTarCacheLocation.replace(/[\\\/]/g, path.posix.sep)

    }

    const debugCmdFlags = debugPort ? `--inspect=0.0.0.0:${debugPort}` : ""

    const runWslServerScript = commitID
        ? "./scripts/wslServer.sh"
        : "./scripts/wslServer-dev.sh"

    const runWslServerCmd = commitID
        ? `"'${runWslServerScript}' '${commitID}' '${quality}' '${serverDataDir}' '${serverTarCacheLocation}' ${debugCmdFlags} ${enableTelemetry ? "" : "--disable-telemetry"}"`
        : `"'${runWslServerScript}' '${appRoot}' ${debugCmdFlags}"`

    const args = []

    if (wslExecutablePath.endsWith("wsl.exe")) {
        if (distro) {
            args.push("-d", distro)
        }
        args.push("bash", "-c", runWslServerCmd)
    } else {
        args.push("-c", runWslServerCmd)
    }

    const launchingWslMessage = new StdoutMessage(`Launching ${wslExecutablePath} ${args.join(" ")} in ${extensionLocation}`)
    sendMessage(launchingWslMessage)

    const wslServer = ChildProcess.spawn(wslExecutablePath, args, {
        cwd: extensionLocation,
        windowsVerbatimArguments: true
    })

    wslServer.stdout.on("data", (chunk) => {
        wslServerStdoutCache += chunk.toString()
        wslSeverMessageReceived = true
    })

    wslServer.stderr.on("data", (chunk) => {
        wslServerStderrCache += chunk.toString()
        wslSeverMessageReceived = true
    })

    wslServer.on("error", (err) => {
        const successful = getWslServerResult()
        if (!successful) {
            resolveError(`VS Code Server for WSL failed with error:\r\n${err.message}`)
        }
    })

    wslServer.on("close", (exitCode) => {
        const result = getWslServerResult();
        if (!result) {
            resolveError("VS Code Server for WSL closed unexpectedly.")
        }
    })

})
