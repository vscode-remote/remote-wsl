
import vscode from "vscode"
import ChildProcess from "child_process"
import path from "path"
import crypto from "crypto"
import os from "os"
import fs from "fs"
import Utils from "./utils"

import { DaemonMessage, DaemonPortResolvedMessage } from "./daemon-message"

let WSLAuthorityResolvePromise: Promise<vscode.ResolvedAuthority> = undefined;

const isDev = !!process.env.VSCODE_DEV
const debugPort = process.env.VSCODE_WSL_AGENT_DEBUG_PORT || (isDev ? "15870" : "")
const daemonScriptPath = path.join(__dirname, "wslDaemon.js")

let terminalRenderer: vscode.TerminalRenderer = undefined
let terminalWriteCache: string[] = []
let wslExists: boolean = undefined

const noMultiDistroContextKey = "remote.wsl.noMultiDistro"
const multiDistroSupported = Utils.isMultiDistroSupported()
const distrosInstalled = Object.create(null)

let savedAuthority: string

const isWslExists = async () => {

    if (typeof wslExists == "boolean") {
        return wslExists
    }

    const wslExecutablePath = Utils.getWSLExecutablePath()

    wslExists = !!wslExecutablePath && fs.existsSync(wslExecutablePath)

    if (!wslExists && wslExecutablePath) {

        writeTerminalMessage(`WSL executable not found at ${wslExecutablePath}`)

        const action = "Documentation"

        const selectedAction = await vscode.window.showErrorMessage(
            "WSL executable not found. Please make sure that WSL is installed.",
            action
        )

        if (selectedAction === action) {
            vscode.env.openExternal(
                vscode.Uri.parse("https://aka.ms/vscode-remote/wsl/install-wsl")
            )
        }

    }

    return wslExists
}

const openVscodeNewWindow = (authority: string) => {
    vscode.commands.executeCommand("vscode.newWindow", {
        remoteAuthority: authority
    })
}

const getAndRegisterDistros = (): Promise<string[]> => {
    return Utils.getDistros().then((distros) => {

        for (const distro of distros) {
            if (!distrosInstalled[distro]) {
                distrosInstalled[distro] = true
            }

            vscode.workspace.registerResourceLabelFormatter({
                scheme: "vscode-remote",
                authority: Utils.getAuthorityFromDistro(distro),
                formatting: {
                    label: "${path}",
                    separator: "/",
                    tildify: true,
                    workspaceSuffix: `WSL: ${distro}`
                }
            })
        }

        return distros

    }, (err) => {
        writeTerminalMessage(err.toString())
        return []
    })
}

const getDistroAuthority = async (skipSelectDistro = false) => {

    if (!multiDistroSupported) {
        return Utils.DEFAULT_AUTHORITY
    }

    const distros = await getAndRegisterDistros()

    if (distros.length !== 0) {

        if (distros.length === 1 && skipSelectDistro) {
            return Utils.getAuthorityFromDistro(distros[0])
        }

        const distroSelected = await vscode.window.showQuickPick(distros, {
            placeHolder: "Select WSL distro"
        })

        if (distroSelected) {
            return Utils.getAuthorityFromDistro(distroSelected)
        } else {
            return
        }

    }

    vscode.window.showInformationMessage(
        "No WSL distros found. New distros can be installed from the Windows Store."
    )

}

const showTerminal = () => {
    if (terminalRenderer) {

        terminalRenderer.terminal.show()

    } else {
        terminalRenderer = vscode.window.createTerminalRenderer("WSL")

        terminalRenderer.terminal.show();

        const writeIntoTerminal = (text: string) => {
            terminalRenderer.write(text)
        }

        terminalWriteCache.forEach(writeIntoTerminal)
        terminalWriteCache.length = 0
    }
}

const writeMultiLineTerminalMessage = (str: string, isError = false) => {
    const messageList = str.split(/\r?\n/)

    for (let i = 0; i < messageList.length; i++) {
        const s = messageList[i]

        if (s.length || i < messageList.length - 1) {
            writeTerminalMessage(s, isError)
        }
    }
}


const writeTerminalMessage = (str: string, isError = false) => {

    const dateStr = new Date()
        .toISOString()
        .replace(/[TZ]/g, " ")
        .trim()

    const text = `[[32m${dateStr}[0m] ${isError ? "[31m" : ""}${str}\r\n`

    if (terminalRenderer) {
        terminalRenderer.write(text)
    } else {
        terminalWriteCache.push(text)
    }

}

export const activate = (context: vscode.ExtensionContext) => {

    const subscriptions = context.subscriptions

    vscode.commands.executeCommand("setContext", noMultiDistroContextKey, !multiDistroSupported);

    const userDataDir = path.dirname(path.dirname(path.dirname(context.logPath)))

    subscriptions.push(
        vscode.workspace.registerRemoteAuthorityResolver(
            "wsl",
            new WSLAuthorityResolver(userDataDir, context.extensionPath)
        )
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.newWindow", async () => {

            if (savedAuthority) {

                openVscodeNewWindow(savedAuthority)

            } else if (await isWslExists()) {

                if (multiDistroSupported) {

                    const distros = await getAndRegisterDistros()

                    if (distros.length === 0) {
                        vscode.window.showInformationMessage(
                            "No WSL distros found. New distros can be installed from the Windows Store."
                        )
                    } else {
                        const authority = Utils.getAuthorityFromDistro(distros[0])
                        openVscodeNewWindow(authority)
                    }

                } else {
                    openVscodeNewWindow(Utils.DEFAULT_AUTHORITY)
                }

            }

        })
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.newWindowForDistro", async () => {

            const WSLExists = await isWslExists()

            if (!WSLExists) {
                return
            }

            if (!multiDistroSupported) {
                const actionSelected = await vscode.window.showInformationMessage(
                    "WSL Remote requires a recent version of WSL (April 2019, build 16299) to support selecting a specific distro. Opening the default distro instead. Use 'wslconfig.exe' to configure the default distro. ",
                    "Ok"
                )

                if (actionSelected) {
                    openVscodeNewWindow(Utils.DEFAULT_AUTHORITY)
                }

                return
            }

            const authority = await getDistroAuthority()

            if (authority) {
                openVscodeNewWindow(authority)
            }

        })
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.showLog", () => {
            showTerminal()
        })
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.getHelp", async () =>
            vscode.env.openExternal(
                vscode.Uri.parse("https://aka.ms/vscode-remote/wsl")
            )
        )
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.getStarted", async () =>
            vscode.env.openExternal(
                vscode.Uri.parse("https://aka.ms/vscode-remote/wsl/getting-started")
            )
        )
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.provideFeedback", async () =>
            vscode.env.openExternal(
                vscode.Uri.parse("https://aka.ms/vscode-remote/wsl/provide-feedback")
            )
        )
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.reportIssue", async () =>
            vscode.commands.executeCommand(
                "vscode.openIssueReporter",
                "ms-vscode-remote.remote-wsl"
            )
        )
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.reopenInWSL", async () => {

            let url = vscode.workspace.workspaceFile || (vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0].uri)

            if (!url || url.scheme !== "file") {
                vscode.window.showInformationMessage(
                    "The current window must be opened on a local workspace file or a folder."
                )
                return
            }

            const authority = await getDistroAuthority(true)

            if (!authority) {
                return
            }

            const wslExecutablePath = Utils.getWSLExecutablePath()

            if (!wslExecutablePath || !fs.existsSync(wslExecutablePath)) {
                vscode.window.showInformationMessage(
                    `'${wslExecutablePath}' not found.\n\nMake sure WSL is installed:\nhttps://aka.ms/vscode-remote/wsl/install-wsl`
                )
                return
            }

            const distro = Utils.getDistroFromAuthority(authority)
            const localPath = url.fsPath

            try {

                const pathInWsl = ChildProcess
                    .execSync(`${wslExecutablePath} ${distro ? "-d " + distro : ""} wslpath -u '${localPath}'`, {
                        stdio: [null, null, null]
                    })
                    .toString()
                    .trim()

                const urlInWsl = url.with({
                    scheme: "vscode-remote",
                    authority: authority,
                    path: pathInWsl
                })

                vscode.commands.executeCommand(
                    "vscode.openFolder",
                    urlInWsl,
                    {
                        forceReuseWindow: true
                    }
                )

            } catch (err) {
                vscode.window.showInformationMessage(
                    `Unable to open ${localPath} in WSL (${distro || "default distro"}). See [WSL log](command:remote-wsl.showLog) for details.`
                )

                writeTerminalMessage(err.toString())

                showTerminal()
            }

        })
    )

    subscriptions.push(
        vscode.commands.registerCommand("remote-wsl.reopenInWindows", async () => {

            let url = vscode.workspace.workspaceFile || (vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders[0].uri)

            if (!url || "vscode-remote" !== url.scheme || "wsl+" !== url.authority.substr(0, 4)) {
                vscode.window.showInformationMessage("The current window must be opened on a workspace file or a folder in WSL.")
                return
            }

            const wslExecutablePath = Utils.getWSLExecutablePath()

            if (wslExecutablePath && fs.existsSync(wslExecutablePath)) {
                try {

                    const distro = Utils.getDistroFromAuthority(url.authority)

                    const pathInWsl = ChildProcess
                        .execSync(
                            `${wslExecutablePath} ${distro ? "-d " + distro : ""} wslpath -w '${url.path}'`,
                            {
                                stdio: [null, null, null]
                            }
                        )
                        .toString()
                        .trim()

                    vscode.commands.executeCommand(
                        "vscode.openFolder",
                        vscode.Uri.file(pathInWsl),
                        {
                            forceReuseWindow: true
                        }
                    )

                } catch (err) {
                    vscode.window.showInformationMessage(`Unable to open ${url.path} in Windows. See [WSL log](command:remote-wsl.showLog) in terminal for details.`)
                    writeTerminalMessage(err.toString())
                    showTerminal()
                }
            } else {
                vscode.window.showInformationMessage(
                    `'${wslExecutablePath}' not found.\n\nMake sure WSL is installed:\nhttps://aka.ms/vscode-remote/wsl/install-wsl`
                )
            }

        })
    )

    if (multiDistroSupported) {
        getAndRegisterDistros()
    }

}

type Progress = vscode.Progress<{
    message?: string;
    increment?: number;
}>

interface Env {
    [key: string]: string | number | boolean | undefined;
}

interface ErrorModalAction {
    title: string;
    execute: Function;
    isCloseAffordance?: boolean;
}

const buildErrorModalActions = () => {

    const actions: ErrorModalAction[] = []

    const isAnyFileUnsaved =
        vscode.workspace.textDocuments.some(document => document.isDirty)
        || (
            vscode.workspace.workspaceFile
            && "untitled" === vscode.workspace.workspaceFile.scheme
        )

    actions.push({
        title: "Retry",
        execute: async () => {
            await vscode.commands.executeCommand(
                "workbench.action.reloadWindow"
            )
        }
    })

    if (!isAnyFileUnsaved) {
        actions.push({
            title: "Close Remote",
            execute: async () => {
                await vscode.commands.executeCommand(
                    "vscode.newWindow",
                    {
                        reuseWindow: true
                    }
                )
            }
        });
    }

    actions.push({
        title: "Ignore",
        isCloseAffordance: true,
        execute: async () => {
            vscode.commands.executeCommand("remote-wsl.showLog");
        }
    })

    return actions

}

const _resolveAuthority = (daemonMessage: DaemonPortResolvedMessage) => {

    const networkInterfaces = os.networkInterfaces()

    const localhostIpAddrs: { [address: string]: true; } = {}

    for (let name in networkInterfaces) {
        for (let info of networkInterfaces[name]) {
            localhostIpAddrs[info.address] = true
        }
    }

    let host = "127.0.0.1"

    const isNotLocalhost = daemonMessage.ipAddresses.length
        && !daemonMessage.ipAddresses.some(addr => localhostIpAddrs[addr])

    if (isNotLocalhost) {
        host = daemonMessage.ipAddresses[0]
    }

    writeTerminalMessage(`WSL resolver response: ${host}:${daemonMessage.port}`)

    return new vscode.ResolvedAuthority(host, daemonMessage.port)

}

class WSLAuthorityResolver implements vscode.RemoteAuthorityResolver {

    _pipeName: string;

    _extensionLocation: string;

    _userDataDir: string;

    constructor(userDataDir: string, extensionLocation: string) {
        this._extensionLocation = extensionLocation
        this._userDataDir = userDataDir
    }

    getPipeName(authority: string) {
        const userIdentify = crypto
            .createHash("sha1")
            .update(this._userDataDir + path.delimiter + authority)
            .digest("hex")

        return `\\\\.\\pipe\\vscode-wsl-${userIdentify}-${vscode.version}-sock`;
    }

    resolve(authority: string) {
        savedAuthority = authority
        return vscode.window.withProgress(
            {
                location: vscode.ProgressLocation.Notification,
                title:
                    "Installing VS Code Server in WSL ([details](command:remote-wsl.showLog))",
                cancellable: false
            },
            (progress) => this._resolve(authority, progress)
        );
    }

    handleInputError(message: string) {
        vscode.window.showErrorMessage(message)

        return Promise.reject(
            vscode.RemoteAuthorityResolverError.NotAvailable(message, true)
        )
    }

    _resolve(authority: string, progress: Progress) {

        if (process.platform !== "win32") {
            return this.handleInputError(
                "WSL is only supported on Windows"
            )
        }

        if (authority === "wsl+Alpine") {
            return this.handleInputError(
                "Alpine is not yet supported by the Remote WSL extension."
            )
        }

        if (authority !== Utils.DEFAULT_AUTHORITY && !Utils.isMultiDistroSupported) {
            return this.handleInputError(
                "Multi distro support requires a newer version of WSL (Windows 10, May 2019 Update) "
            )
        }

        if (WSLAuthorityResolvePromise) {
            return WSLAuthorityResolvePromise
        }

        writeTerminalMessage(`Starting VS Code Server inside WSL (${Utils.getDistroFromAuthority(authority) || "default distro"})`)

        writeTerminalMessage(`Windows build: ${Utils.getWindowsBuildNumber()}. Multi distro support: ${Utils.isMultiDistroSupported ? "enabled" : "disabled"}`)

        progress.report({
            message: "Starting Linux Subsystem"
        })

        let exited = false

        WSLAuthorityResolvePromise = new Promise((resolve, reject) => {

            let resolveError = async (message: string) => {

                writeTerminalMessage(message, true)

                if (!exited) {

                    exited = true

                    writeTerminalMessage("For help with startup problems, go to")

                    writeTerminalMessage(
                        "https://code.visualstudio.com/docs/remote/troubleshooting#_wsl-tips"
                    )

                    showTerminal()

                    message += "\r\nCheck WSL terminal for more details."

                    const selectedAction = await vscode.window.showErrorMessage(message, { modal: true }, ...buildErrorModalActions())

                    if (selectedAction) {
                        await selectedAction.execute()
                    }

                    reject(vscode.RemoteAuthorityResolverError.NotAvailable(message, true))

                }
            }

            let resolveDaemonMessage = (daemonMessage: DaemonMessage) => {
                switch (daemonMessage.type) {
                    case "progressMessage":
                        progress.report({
                            increment: daemonMessage.increment,
                            message: daemonMessage.message
                        });
                        break;

                    case "stdoutMessage":
                        writeMultiLineTerminalMessage(daemonMessage.data);
                        break;

                    case "stderrMessage":
                        writeMultiLineTerminalMessage(daemonMessage.data, true);
                        break;

                    case "portResolved":

                        if (!exited) {

                            exited = true

                            resolve(
                                _resolveAuthority(daemonMessage)
                            )

                        }

                        break;

                    case "fatalErrorOccured":
                        resolveError(daemonMessage.message);
                        break;

                    default:
                        throw new Error("Received unexpected message!");
                }
            }

            let daemonOutputCache = ""

            try {

                const remoteEnv: Env = {}

                Object.keys(process.env).forEach(key => {
                    remoteEnv[key] = process.env[key]
                })

                remoteEnv.VSCODE_WSL_AGENT_DEBUG_PORT = debugPort
                remoteEnv.VSCODE_WSL_PIPE_NAME = this.getPipeName(authority)
                remoteEnv.VSCODE_WSL_EXT_HOST_PID = process.pid
                remoteEnv.VSCODE_WSL_EXT_LOCATION = this._extensionLocation
                remoteEnv.VSCODE_WSL_APP_ROOT = vscode.env.appRoot

                if (Utils.isMultiDistroSupported() && authority !== Utils.DEFAULT_AUTHORITY) {
                    remoteEnv.VSCODE_WSL_DISTRO = Utils.getDistroFromAuthority(authority)
                }

                const enableTelemetry: boolean = vscode.workspace
                    .getConfiguration()
                    .get("telemetry.enableTelemetry")

                if (enableTelemetry) {
                    remoteEnv.VSCODE_WSL_ENABLE_TELEMETRY = true
                }

                const forkoptions: ChildProcess.ForkOptions = {
                    stdio: ["pipe", "pipe", "pipe", "ipc"],
                    detached: true,
                    // @ts-ignore
                    env: remoteEnv,
                    execArgv: [],
                    cwd: process.cwd()
                }

                const daemon = ChildProcess.fork(daemonScriptPath, [], forkoptions)

                const resolveMultiLineDaemonOutput = () => {

                    let lineFeedIndex: number

                    while ((lineFeedIndex = daemonOutputCache.indexOf("\n")) > 0) {

                        let daemonMessage: DaemonMessage = JSON.parse(
                            daemonOutputCache.substr(0, lineFeedIndex)
                        )

                        daemonOutputCache = daemonOutputCache.substr(lineFeedIndex + 1)

                        resolveDaemonMessage(daemonMessage)

                    }

                }

                daemon.stdout.on("data", (chunk) => {
                    daemonOutputCache += chunk.toString()
                    resolveMultiLineDaemonOutput()
                })

                daemon.stderr.on("data", (chunk) => {
                    writeMultiLineTerminalMessage(chunk.toString())
                })

                daemon.on("error", (err) => {
                    resolveError(String(err))
                })

                daemon.on("close", (exitCode) => {
                    resolveError(`WSL Daemon exited with code ${exitCode}`)
                })

            } catch (err) {
                const message = (err && err.message) || ""
                resolveError("Problems starting WSL Daemon: " + message)
            }
        })

        return WSLAuthorityResolvePromise
    }
}

export default {
    activate,
}
