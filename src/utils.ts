

import path from "path"
import os from "os"
import fs from "fs"
import ChildProcess from "child_process"
import https from "https"
import util from "util"

const fileExists = util.promisify(fs.exists)
const mkdir = util.promisify(fs.mkdir)
const rename = util.promisify(fs.rename)

interface ProductInfo {
    commit: string;
    quality: string;
    serverDataFolderName?: string;
    updateUrl: string;
}

let productInfo: ProductInfo

export const getWSLConfigExecutablePath = () => {

    const is32BitProgram = process.env.hasOwnProperty("PROCESSOR_ARCHITEW6432")
    const SystemRoot = process.env.SystemRoot

    if (SystemRoot) {
        return path.join(
            SystemRoot,
            is32BitProgram ? "Sysnative" : "System32",
            "wslconfig.exe"
        )
    }

}

export const getWindowsBuildNumber = () => {
    const versionRegex = /(\d+)\.(\d+)\.(\d+)/g
    const match = versionRegex.exec(os.release())

    let n = 0
    if (match && match.length === 4) {
        n = parseInt(match[3])
    }

    return n
}

export const isMultiDistroSupported = () => {
    const windowsBuildNumber = getWindowsBuildNumber()
    return windowsBuildNumber >= 18362
}

export const getWSLExecutablePath = () => {

    let isNewerThan1709 = getWindowsBuildNumber() >= 16299  // windows 10 1709

    const is32BitProgram = process.env.hasOwnProperty("PROCESSOR_ARCHITEW6432")
    const SystemRoot = process.env.SystemRoot

    if (SystemRoot) {
        return path.join(
            SystemRoot,
            is32BitProgram ? "Sysnative" : "System32",
            isNewerThan1709 ? "wsl.exe" : "bash.exe"
        )
    }

}

export const getProductConfiguration = (appRoot: string) => {
    if (!productInfo) {
        const filePath = path.join(appRoot, "product.json")
        const data = fs.readFileSync(filePath).toString()
        productInfo = JSON.parse(data)
    }

    return productInfo
}

const printableCharactersRegex = /^[\w\d+\.\-_~!$&'\(\)\*\+,;=]+$/
const charCodeOfPlusSign = "+".charCodeAt(0)

export const DEFAULT_AUTHORITY = "wsl+default"

export const getAuthorityFromDistro = (distro: string) => {
    if (distro) {

        const usePlainTextDistroName = printableCharactersRegex.test(distro) && "default" !== distro && distro.charCodeAt(0) !== charCodeOfPlusSign

        if (usePlainTextDistroName) {
            return "wsl+" + distro
        } else {
            return "wsl++" + Buffer.from(distro, "utf8").toString("hex")
        }

    } else {
        return DEFAULT_AUTHORITY
    }
}

export const getDistroFromAuthority = (authority: string) => {

    if (/^wsl\+/.test(authority)) {

        const distro = authority.substr(4)

        if (distro === "default") {
            return
        } else if (distro.charCodeAt(0) === charCodeOfPlusSign) {
            return Buffer.from(distro.substr(1), "hex").toString("utf8")
        } else {
            return distro
        }

    }

    throw new Error("authority must start with wsl+")

}

export const getDistros = (): Promise<string[]> => {
    return new Promise((resolve, reject) => {

        const wslConfigExecutablePath = getWSLConfigExecutablePath()

        if (wslConfigExecutablePath) {

            ChildProcess.execFile(wslConfigExecutablePath, ["/list"],
                {
                    windowsVerbatimArguments: !0,
                    encoding: "utf16le"
                },
                (err, stdout, stderr) => {
                    if (err) {
                        reject(err)
                    } else {

                        const EOL = /\r\n/.test(stdout) ? "\r\n" : "\n"
                        const lines = stdout.split(EOL)

                        let distros: string[] = []

                        for (let i = 1; i < lines.length; i++) {
                            let distroMatch = lines[i].match(/^([^\s]+)/)
                            if (distroMatch) {
                                distros.push(distroMatch[1])
                            }
                        }

                        resolve(distros)

                    }
                }
            )

        } else {
            reject(new Error("Unable to find location of wslconfig.exe"))
        }

    })
}

const mkdirp = async (p: string) => {
    if (!await fileExists(p)) {
        await mkdirp(path.dirname(p))
        await mkdir(p)
    }
}

type ProgressReportFn = (value: number, max?: number) => any

const download = (url: string, savePath: string, progressReportFn: ProgressReportFn, redirectLimit = 5): Promise<void> => {
    return new Promise((resolve, reject) => {

        const saveFileStream = fs.createWriteStream(savePath)

        const req = https.get(url, (res) => {

            const isRedirection = (res.statusCode >= 300 && res.statusCode <= 303) || res.statusCode === 307

            if (redirectLimit > 0 && isRedirection) {
                let redirectLocation = res.headers.location

                if (redirectLocation) {
                    download(redirectLocation, savePath, progressReportFn, redirectLimit - 1).then(resolve, reject)
                    return
                }
            }

            if (res.statusCode < 200 || res.statusCode > 299) {
                reject(`Failed to download VS Code Server from ${url}: HTTP ${res.statusCode} - ${res.statusMessage}`)
                return
            }

            const contentLength = parseInt(res.headers["content-length"] || "0")

            let downloadedSize = 0  // byte

            if (contentLength) {
                res.on("data", chunk => {
                    downloadedSize += chunk.length
                    progressReportFn(downloadedSize, contentLength)
                })
            }

            res.on("error", reject)

            res.pipe(saveFileStream)

            res.on("end", resolve)

        })

        req.on("error", reject)

    })
}

export const donwloadBuild = async (productInfo: ProductInfo, savePath: string, progressReportFn: ProgressReportFn) => {

    await mkdirp(path.dirname(savePath))

    const url = `${productInfo.updateUrl}/commit:${productInfo.commit}/server-linux-x64/${productInfo.quality || "insider"}`

    const cachePath = `${savePath}_${Date.now()}`

    return download(url, cachePath, progressReportFn).then(_ =>
        rename(cachePath, savePath)
            .then(_ => savePath)
    )

}

export const getServerTarCacheLocation = (productInfo: ProductInfo) => {
    return path.join(
        os.tmpdir(),
        "vscode-remote-wsl",
        productInfo.commit,
        "vscode-server-linux-x64.tar.gz"
    )
}

export default {
    isMultiDistroSupported,
    getWSLExecutablePath,
    getWSLConfigExecutablePath,
    getProductConfiguration,
    getWindowsBuildNumber,
    DEFAULT_AUTHORITY,
    getAuthorityFromDistro,
    getDistroFromAuthority,
    getDistros,
    donwloadBuild,
    getServerTarCacheLocation,
}
