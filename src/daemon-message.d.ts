
export interface DaemonProgressMessage {
    type: "progressMessage";
    increment: number;
    message: string;
}

export interface DaemonStdoutMessage {
    type: "stdoutMessage";
    data: string;
}

export interface DaemonStderrMessage {
    type: "stderrMessage";
    data: string;
}

export interface DaemonPortResolvedMessage {
    type: "portResolved";
    ipAddresses: string[];
    port: number;
}

export interface DaemonFatalErrorOccuredMessage {
    type: "fatalErrorOccured";
    message: string;
}

export type DaemonMessage =
    DaemonProgressMessage
    | DaemonStdoutMessage
    | DaemonStderrMessage
    | DaemonPortResolvedMessage
    | DaemonFatalErrorOccuredMessage

export default DaemonMessage
